package xSiZero;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Aplicatie {
	private static List<JButton> toateButoanele = new ArrayList<>();
	static private JButton buton_1 = daButon();
	static private JButton buton_2 = daButon();
	static private JButton buton_3 = daButon();
	static private JButton buton_4 = daButon();
	static private JButton buton_5 = daButon();
	static private JButton buton_6 = daButon();
	static private JButton buton_7 = daButon();
	static private JButton buton_8 = daButon();
	static private JButton buton_9 = daButon();
	private static int contorizare = 0;
	private static int nrRunde = 0;

	static Map<String, Integer> campionat = new HashMap<>();

	static private JTextArea textArea = new JTextArea();
	static private JTextField fieldRunde = new JTextField("5");

	static private JLabel scorX = new JLabel("0");
	static private JLabel scor0 = new JLabel("0");

	private static JButton daButon() {
		final JButton buton = new JButton();
		buton.setText(" ");
		buton.setPreferredSize(new Dimension(55, 68));
		buton.addActionListener(daListener(buton));

		toateButoanele.add(buton);

		return buton;
	}

	static private final ActionListener daListener(final JButton buton) {
		return new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				final Runnable runnable = (Runnable) Toolkit
						.getDefaultToolkit().getDesktopProperty(
								"win.sound.exclamation");
				if (runnable != null)
					runnable.run();

				if ((buton.getText() == null)
						|| (buton.getText().trim().isEmpty())) {
					++contorizare;
					if ((contorizare % 2) == 0) {
						buton.setText("0");
					} else {
						buton.setText("X");
					}

					if (calculeaza(daLinie(buton_1, buton_2, buton_3),
							"prima linie")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_4, buton_5, buton_6),
							"a doua linie")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_7, buton_8, buton_9),
							"a treia linie")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_1, buton_4, buton_7),
							"prima coloana")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_2, buton_5, buton_8),
							"a doua coloana")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_3, buton_6, buton_9),
							"a treia coloana")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_1, buton_5, buton_9),
							"prima diagonala")) {
						stopJoc();
					} else if (calculeaza(daLinie(buton_3, buton_5, buton_7),
							"a doua diagonala")) {
						stopJoc();
					} else if (contorizare == 9) {
						++nrRunde;
						addMesaj("Remiza.");
					}
				} else {
					addMesaj("Warning! mutare interzisa, aici avem deja "
							+ buton.getText());
				}
			}
		};
	}

	static private void addMesaj(String noulMesaj) {
		textArea.setText(textArea.getText() + "\n\n" + noulMesaj);

		// textArea.setText(noulMesaj);
	}

	static private final void stopJoc() {
		addMesaj(" felicitari castigatorului, jocul s-a terminat!");

		// System.exit(1);
	}

	private static final boolean calculeaza(String rezultat, String pozitie) {

		boolean returnResults = false;

		if (rezultat.equals("000")) {

			int scor = new Integer(scor0.getText());
			++scor;
			scor0.setText(scor + "");

			campionat.put("0", campionat.get("0") + 1);
			addMesaj(" 0 pe " + pozitie + " , jucatorul 2 a castigat!");
			removeAllListener();
			returnResults = true;
		} else if (rezultat.equals("XXX")) {
			int scor = new Integer(scorX.getText());
			++scor;
			scorX.setText(scor + "");
			campionat.put("X", campionat.get("X") + 1);
			addMesaj(" X pe " + pozitie + " , jucatorul 1 a castigat!");
			removeAllListener();
			returnResults = true;
		}

		if (returnResults) {
			++nrRunde;

			int nrRunda = new Integer(fieldRunde.getText());

			if (nrRunde == nrRunda) {
				String mesaj = "Final campionat, scorul este:\n";
				mesaj += " X: " + campionat.get("X") + "\n";
				mesaj += " 0: " + campionat.get("0");
				addMesaj(mesaj);
			}
		}

		return returnResults;
	}

	private static final String daLinie(JButton b1, JButton b2, JButton b3) {

		return b1.getText() + b2.getText() + b3.getText();
	}

	static private void removeAllListener() {

		if ((toateButoanele != null) && !toateButoanele.isEmpty()) {
			for (JButton buton : toateButoanele) {
				removeListener(buton);
			}
		}
	}

	static private void removeListener(JButton buton) {
		ActionListener[] listaL = buton.getActionListeners();
		if ((listaL != null) && (listaL.length > 0)) {
			for (ActionListener listen : listaL) {
				buton.removeActionListener(listen);
			}
		}
	}

	private static void createAndShowGUI() {
		try {
			campionat.put("X", 0);
			campionat.put("0", 0);

			fieldRunde.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (!fieldRunde.getText().matches("\\d+")){
						addMesaj("Valoarea nu e numerica, ramane 5");
						fieldRunde.setText("5");
					}
				}
			});
			JButton restartJoc = new JButton("restart joc");
			restartJoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					contorizare = 0;
					removeAllListener();

					if ((toateButoanele != null) && !toateButoanele.isEmpty()) {
						for (JButton buton : toateButoanele) {
							buton.setText(" ");
							buton.addActionListener(daListener(buton));
						}
					}
					textArea.setText("");

					int nrRunda = new Integer(fieldRunde.getText());
					if (nrRunde == nrRunda) {
						scor0.setText("0");
						scorX.setText("0");
					}
				}
			});

			JScrollPane scroll = new JScrollPane(textArea,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

			final JFrame frame = new JFrame("HelloWorldSwing");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLayout(new BorderLayout());
			frame.setPreferredSize(new Dimension(700, 245));
			frame.setResizable(false);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			final JPanel panelPrincipal = new JPanel();
			panelPrincipal.setLayout(new GridBagLayout());

			panelPrincipal.add(buton_1, new GridBagConstraints(0, 0, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_2, new GridBagConstraints(1, 0, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_3, new GridBagConstraints(2, 0, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_4, new GridBagConstraints(0, 1, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_5, new GridBagConstraints(1, 1, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_6, new GridBagConstraints(2, 1, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_7, new GridBagConstraints(0, 2, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_8, new GridBagConstraints(1, 2, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(buton_9, new GridBagConstraints(2, 2, 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			panelPrincipal.add(new JLabel("X:"),
					new GridBagConstraints(0, 3, 1, 1, 1, 1,
							GridBagConstraints.FIRST_LINE_START,
							GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5,
									5), 0, 0));

			panelPrincipal.add(scorX, new GridBagConstraints(1, 3, 0, 0, 0, 0,
					GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.HORIZONTAL, new Insets(5, -20, 5, 5), 0,
					0));
			panelPrincipal.add(new JLabel("0:"),
					new GridBagConstraints(0, 4, 1, 1, 1, 1,
							GridBagConstraints.FIRST_LINE_START,
							GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5,
									5), 0, 0));
			panelPrincipal.add(scor0, new GridBagConstraints(1, 4, 0, 0, 0, 0,
					GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.HORIZONTAL, new Insets(5, -20, 5, 5), 0,
					0));

			panelPrincipal.add(new JLabel("nr. runde"), new GridBagConstraints(
					0, 5, 0, 0, 1, 1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(0, 5, 5, 5), 0, 0));
			panelPrincipal.add(fieldRunde, new GridBagConstraints(1, 5, 0, 0,
					1, 1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(0, 20, 5, 5), 25, 0));
			panelPrincipal.add(restartJoc, new GridBagConstraints(0, 6, 0, 0,
					0, 0, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(25, 5, 5, 5), 0, 0));
			panelPrincipal.add(scroll, new GridBagConstraints(3, 0, 0, 0, 0, 0,
					GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0,
					177));
			panelPrincipal.add(new JLabel(""), new GridBagConstraints(9, 9, 3,
					3, 777, 777, GridBagConstraints.CENTER,
					GridBagConstraints.CENTER, new Insets(5, 5, 5, 5), 0, 0));
			frame.add(panelPrincipal, BorderLayout.CENTER);

			frame.pack();
			frame.setVisible(true);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}