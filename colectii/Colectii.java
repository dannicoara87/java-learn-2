package colectii;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Colectii {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> listaSimpla = new ArrayList<>(); // permite duplicate
		
		listaSimpla.add("mere");
		listaSimpla.add("pere");
		listaSimpla.add("banane");
		listaSimpla.add("portocale");
		listaSimpla.add("lamai");
		listaSimpla.add("mango");
		
		for (String a : listaSimpla){
			System.out.println(a);
		}
		
		Set<String> listaSimpla2 = new HashSet<>(); // nu permite duplicate
		
		listaSimpla2.add("mere");
		listaSimpla2.add("mere");
		listaSimpla2.add("pere");
		
		Set<String> listaSimpla3 = new TreeSet<>(); // nu permite duplicate, ordoneaza alfabetic
		
		listaSimpla3.add("mere");
		listaSimpla3.add("portocale");
		listaSimpla3.add("pere");
	}

}
