package abstractizari2;

public abstract class ClasaAbstracta {
	
	abstract void testAfisare3();
	protected abstract void testAfisare();
	//private abstract void testAfisare4(); // nu permite pmetode abstracte private
	//final abstract void testAfisare4(); // nu permite pmetode abstracte finale
	
	void testAfisare2(){};
	
	public static void main(String[] args) {
		final int b = 7;
		
		//b = 9; //nu permite, e var final
		
		// clasele abstracte nu pot fi instantiate
		//ClasaAbstracta obj1 = new ClasaAbstracta();
	}

}
