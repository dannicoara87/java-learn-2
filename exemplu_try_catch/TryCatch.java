package exemplu_try_catch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TryCatch {
	
	static void test(){
		Map<String, Animal> animal = new HashMap<>();
		Ghepard ghepard = new Ghepard();
		animal.put("leu", new Leu());
		animal.put("pisica", new Pisica());
		animal.put("ghepard", new Ghepard());
		animal.put("ghepard", ghepard);
		animal.put("ghepard", ghepard);
		animal.put("ghepard", ghepard);
		//		System.out.println(animal.size());
		//		
		//		for(String cheie : animal.keySet()){
		//			System.out.println();
		//			System.out.print(cheie + "  ");
		//			animal.get(cheie).mananca();	
		//		}
		try{
			System.out.println("se incearca codul");
			//Pisica pisicel = (Pisica)animal.get("lleu");
            //System.exit(1);
			//int a = 100/1;
			int a = (7*24 - 3) + 5;
			if ( a > 0) throw new Exception(" calculul nu e negativ cum ne asteptam");
//			System.out.println("BBBBBBBBBB");
//			pisicel.mananca();
			System.out.println("s-a incercat codul, e OK");
		}
		catch(ClassCastException e){
			System.out.println("am intrat in blocul catch, ClassCastException");
			e.printStackTrace();
			System.out.println("cauza:");
			System.out.print(e.getCause());
		}
		catch(Exception e){
			System.out.println("am intrat in blocul catch, Exception");
			e.printStackTrace();
			System.out.println("cauza:");
			System.out.print(e.getCause());
		}
		catch(Throwable e){
			System.out.println("am intrat in blocul catch, Throwable");
			e.printStackTrace();
			System.out.println("cauza:");
			System.out.print(e.getCause());
		}
		
		
		finally{
			//System.out.println("execut finally");
		}	
	}
	
	public static void main(String[] args) {
		
		test();

		System.out.println("terminat static void main");
	}
}