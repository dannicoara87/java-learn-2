package cast;

public class ClasaPrincipala {

	public static void main(String[] args) {
		
		Animal obj1 = new Pisica();
		
		// de testat acasa
		//Animal obj1 = new Animal();
		//Pisica obj1 = new Pisica();
		
		// comportament default
		System.out.println(obj1.tipAnimal);
		System.out.println(obj1.varsta);
		//System.out.println(obj1.masa);
		obj1.mananca();
		obj1.culoare();
		//obj1.miauna();
		
		
		//up-cast
//		System.out.println(((Animal)obj1).tipAnimal);
//		System.out.println(((Animal)obj1).varsta);
//		((Animal)obj1).mananca();
//		((Animal)obj1).culoare();
		
		//down-cast
//		System.out.println(((Pisica)obj1).tipAnimal);
//		System.out.println(((Pisica)obj1).varsta);
//		System.out.println(((Pisica)obj1).nume);
//		((Pisica)obj1).mananca();
//		((Pisica)obj1).culoare();
//		
//		((Pisica)obj1).miauna();
		
	}

}
