package polimorfism;

public class ClasaPrincipala {
	
	static void animalulMananca(Animal a){
		a.mananca();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Felina pisica_1 = new Pisica();
		Felina leu_1 = new Leu();
		
		//System.out.print(pisica_1.greutate); pisica_1.mananca();
		//System.out.print(leu_1.greutate); leu_1.mananca();
		
		animalulMananca(pisica_1);
		animalulMananca(leu_1);
		
	}
		

}
