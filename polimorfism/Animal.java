package polimorfism;

public interface Animal {
	public static final int aa = 44; // variabilele din interfata sunt public static final
	
	public abstract void mananca(); // metodele sunt public abstract
}
