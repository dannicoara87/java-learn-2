package proiect1;

import java.util.List;

public class ClasaB {
	
	void parcurgeListaCuFor(List<String> Lista1){
		
		if(Lista1 != null && !Lista1.isEmpty()){
			
			// parcurgem lista
			for (String element : Lista1){
				System.out.println(element);
			}
		}else{
			System.out.println("Lista e nula");
		}
		
	}
}
