package proiect1;

import java.util.ArrayList;
import java.util.List;

public class ClasaA extends Object{
	
	static List<String> lista2; // valoare implicita: null
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClasaB instanta1 = new ClasaB();
		List<String> lista1 = new ArrayList<>();
		
		lista1.add("patrat");
		lista1.add("triunghi");
		lista1.add("cerc");
		lista1.add("dreptunghi");
		lista1.add("trapez");
		
		//instanta1.parcurgeListaCuFor(lista1);
		instanta1.parcurgeListaCuFor(lista2);
	}

}
