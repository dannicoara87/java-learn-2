package proiect1;

import java.util.List;

public class ClasaTest {
	
	private String numePlanta = "lalea";
	boolean numeVariabila = true;
	
	void afis(int a){
		System.out.println("parametrul primit este: "+ a + " - " + numePlanta);
	}
	
	void invatSaNumar(int max){
		if(max >= 0){
			for (int i = 0; i <= max; ++i){
				System.out.println(i);
			}
		}else{
			System.out.println("Nu vreau sa numar pentru numere negative");
		}
	}
	
	void afisezUnVector(String [] a){
		if(a.length > 0){
			for (int i = 0; i < a.length; ++i){
				System.out.println(a[i]);
			}
		}
	}
	
	void afisareFaraIndex(List<String> a){
		
		if(a.size() > 0){
			for(String planta : a){
				System.out.println(planta);
			}
		}
	}
}
