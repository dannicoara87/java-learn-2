package proiect1;

import java.util.ArrayList;
import java.util.List;

public class PrimaClasa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello world");

		ClasaTest numeInstanta = new ClasaTest();
		//System.out.println(numeInstanta.numeVariabila);
		//numeInstanta.afis(98);
		
		//numeInstanta.invatSaNumar(-99);
		//numeInstanta.invatSaNumar(7);
		
		String[] vectorPasat = {"garoafa", "lalea", "trandafir"};
		numeInstanta.afisezUnVector(vectorPasat);
		
		List<String> listaMea = new ArrayList<>();
		
		listaMea.add("garoafa");
		listaMea.add("lalea");
		listaMea.add("trandafir");
		listaMea.add("crizantema");
		
		numeInstanta.afisareFaraIndex(listaMea);
	}
}
