package proiect1;

public class C extends B{
	
	static int CC = daCC();
	
	static int daCC(){
		return 77;
	}

	String qq = daQQ();
	
	String daQQ(){
		return "qqqqq";
	}	
	
	static{
		System.out.println("zona statica in C");
	}
	
	{
		System.out.println("zona dinamica in C");
	}
	
	public C(){
		this(7);
	}

	public C(int a){
		super(7);
	}
	
	public static void main(String[] args){
		System.out.println("incepe distractia");
		
		System.out.println("Gata bucla statica");
		new C();
		
		System.out.println("Gata bucla 1");
		
		new C(222);
		
		System.out.println("Gata bucla 2");
		
		new B();
		
		System.out.println("Gata bucla 3");

	}

}
