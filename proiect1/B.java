package proiect1;

public class B extends A{
	
	static int BB = daBB();
	
	static int daBB(){
		return 77;
	}

	String qq1 = daQQ1();
	
	String daQQ1(){
		return "qqqqq1";
	}	
	
	static{
		System.out.println("zona statica in B");
	}
	
	{
		System.out.println("zona dinamica in B");
	}
	
	public B(){
		this(8);
		System.out.println("clasa B, constructor 1");
	}

	public B(int a){
		super(8);
		System.out.println("clasa B, constructor 2");
	}	
}
