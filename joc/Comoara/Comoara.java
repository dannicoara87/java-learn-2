package joc.Comoara;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public final class Comoara {
	private static final Map<JButton, Integer> toateButoanele = new HashMap<>();
	private static final Map<JButton, Integer> distantaComoara = new HashMap<>();
	private static final Map<JButton, List<Integer>> mutariValide = new HashMap<>();
	static private JTextArea textArea = new JTextArea();
	static int  comoara = 0;
	private static boolean firstClick = true;
	private static int butonAnterior = 0;
	private static List<Integer> primaLinie = new ArrayList<>();
	private static List<Integer> ultimaLinie = new ArrayList<>();
	private static List<Integer> primaColoana = new ArrayList<>();
	private static List<Integer> ultimaColoana = new ArrayList<>();

	private static void coloreazaButon(JButton buton){
		int nrButon = toateButoanele.get(buton);

		if (nrButon == comoara){
			removeAllListener();
			buton.setBackground(Color.MAGENTA);
		}
		else {
			buton.setBackground(Color.BLUE);
		}
	}

	private static void calculeazaDistantaComoara(JButton buton){
		int distanta = distantaComoara.get(buton);
		if (distanta != 0){
			textArea.setText(textArea.getText() + "\n" + "Distanta fata de comoara: " + distanta);
		}
		else {
			textArea.setText(textArea.getText() + "\n" + "Felicitari, ai gasit comoara!");
		}
	}
	static private void removeAllListener() {

		if ((toateButoanele != null) && !toateButoanele.isEmpty()) {
			for (JButton buton : toateButoanele.keySet()) {
				removeListener(buton);
			}
		}
	}
	static private void removeListener(JButton buton) {
		ActionListener[] listaL = buton.getActionListeners();
		if ((listaL != null) && (listaL.length > 0)) {
			for (ActionListener listen : listaL) {
				buton.removeActionListener(listen);
			}
		}
	}
	static private final ActionListener daListener(final JButton buton) {
		return new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				final Runnable runnable = (Runnable) Toolkit
						.getDefaultToolkit().getDesktopProperty(
								"win.sound.exclamation");
				if (runnable != null)
					runnable.run();

				if (firstClick){
					firstClick = false;
					butonAnterior = toateButoanele.get(buton);
					calculeazaDistantaComoara(buton);
					coloreazaButon(buton);
				}
				else {
					List<Integer> pasiValizi = mutariValide.get(buton);
					if (pasiValizi.contains(butonAnterior)){
						butonAnterior = toateButoanele.get(buton);
						calculeazaDistantaComoara(buton);
						coloreazaButon(buton);
					}
					else {
						textArea.setText(textArea.getText() + "\n" +"mutare INVALIDA");
					}
				}
			}
		};
	}
	private static JButton daButon(int i) {
		final JButton buton = new JButton();
		buton.setPreferredSize(new Dimension(55, 68));
		buton.setText(" ");
		buton.setBackground(Color.GREEN);
		buton.addActionListener(daListener(buton));
		toateButoanele.put(buton, i);

		if (i == 1) {
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(2);
			listaPasi.add(12);
			listaPasi.add(11);
			mutariValide.put(buton, listaPasi);
		}
		else if (i == 10) {
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(9);
			listaPasi.add(19);
			listaPasi.add(20);
			mutariValide.put(buton, listaPasi);
		}
		else if (i == 100) {
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(89);
			listaPasi.add(90);
			listaPasi.add(99);
			mutariValide.put(buton, listaPasi);
		}
		else if (i == 91) {
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(81);
			listaPasi.add(82);
			listaPasi.add(92);
			mutariValide.put(buton, listaPasi);
		}
		else if (primaLinie.contains(i)){
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(i+1);
			listaPasi.add(i-1);
			listaPasi.add(i+9);
			listaPasi.add(i+10);
			listaPasi.add(i+11);
			mutariValide.put(buton, listaPasi);
		}
		else if (ultimaLinie.contains(i)){
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(i+1);
			listaPasi.add(i-1);
			listaPasi.add(i-9);
			listaPasi.add(i-10);
			listaPasi.add(i-11);
			mutariValide.put(buton, listaPasi);
		}
		else if (primaColoana.contains(i)){
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(i-10);
			listaPasi.add(i+10);
			listaPasi.add(i-9);
			listaPasi.add(i+1);
			listaPasi.add(i+11);
			mutariValide.put(buton, listaPasi);
		}
		else if (ultimaColoana.contains(i)){
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(i-10);
			listaPasi.add(i+10);
			listaPasi.add(i-11);
			listaPasi.add(i-1);
			listaPasi.add(i+9);
			mutariValide.put(buton, listaPasi);
		}
		else {
			List<Integer> listaPasi = new ArrayList<Integer>();
			listaPasi.add(i-1);
			listaPasi.add(i+1);
			listaPasi.add(i-10);
			listaPasi.add(i+10);
			listaPasi.add(i-9);
			listaPasi.add(i+9);
			listaPasi.add(i-11);
			listaPasi.add(i+11);
			mutariValide.put(buton, listaPasi);
		}

		return buton;
	}
	private static void adaugaElemente(){

		primaLinie.add(2);
		primaLinie.add(3);
		primaLinie.add(4);
		primaLinie.add(5);
		primaLinie.add(6);
		primaLinie.add(7);
		primaLinie.add(8);
		primaLinie.add(9);

		ultimaLinie.add(92);
		ultimaLinie.add(93);
		ultimaLinie.add(94);
		ultimaLinie.add(95);
		ultimaLinie.add(96);
		ultimaLinie.add(97);
		ultimaLinie.add(98);
		ultimaLinie.add(99);

		primaColoana.add(11);
		primaColoana.add(21);
		primaColoana.add(31);
		primaColoana.add(41);
		primaColoana.add(51);
		primaColoana.add(61);
		primaColoana.add(71);
		primaColoana.add(81);

		ultimaColoana.add(20);
		ultimaColoana.add(30);
		ultimaColoana.add(40);
		ultimaColoana.add(50);
		ultimaColoana.add(60);
		ultimaColoana.add(70);
		ultimaColoana.add(80);
		ultimaColoana.add(90);

	}
	private static void createAndShowGUI(){

		adaugaElemente();

		comoara = (int) (Math.random() * 99) + 1;
		JScrollPane scroll = new JScrollPane(textArea,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		final JFrame frame = new JFrame("Comoara");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setPreferredSize(new Dimension(700, 400));
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());

		int linie = 0;
		int coloana = 0;
		int contor = 0;
		for (int i = 1 ; i<=100 ; i++){
			panelPrincipal.add(daButon(i), new GridBagConstraints(linie ,coloana , 1, 1, 1,
					1, GridBagConstraints.FIRST_LINE_START,
					GridBagConstraints.FIRST_LINE_START,
					new Insets(5, 5, 5, 5), 0, 0));
			++contor;
			++linie;
			if ((contor % 10) == 0){
				++coloana;
				linie = 0;
			}
		}

		panelPrincipal.add(scroll, new GridBagConstraints(11, 0, 0, 0, 0, 0,
				GridBagConstraints.FIRST_LINE_START,
				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 11,
				333));

		panelPrincipal.add(new JLabel(""), new GridBagConstraints(44, 44, 3,
				3, 777, 777, GridBagConstraints.CENTER,
				GridBagConstraints.CENTER, new Insets(5, 5, 5, 5), 0, 0));
		frame.add(panelPrincipal, BorderLayout.CENTER);

		frame.pack();
		frame.setVisible(true);

		if ((toateButoanele != null) && !toateButoanele.isEmpty()){
			for (JButton buton : toateButoanele.keySet()){
				if (toateButoanele.containsKey(buton)){
					int nrButon = toateButoanele.get(buton);
					distantaComoara.put(buton, Math.abs(comoara - nrButon));
				}		
			}
		}
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}