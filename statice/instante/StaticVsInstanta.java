package statice.instante;

public class StaticVsInstanta {
	
	static int varstatic = 7;
	int vardeinstanta = 100;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StaticVsInstanta ob1 = new StaticVsInstanta();
		StaticVsInstanta ob2 = new StaticVsInstanta();
		
		System.out.println(" obj 1 var stat "+ ob1.varstatic);
		System.out.println(" obj 1 var instanta "+ ob1.vardeinstanta);
		System.out.println(" obj 2 var stat "+ ob2.varstatic);
		System.out.println(" obj 2 var instanta "+ ob2.vardeinstanta);
		
		ob1.varstatic = 8;
		ob1.vardeinstanta = 101;
		
		
		
		System.out.println(" obj 1 var stat "+ ob1.varstatic);
		System.out.println(" obj 1 var instanta "+ ob1.vardeinstanta);
		System.out.println(" obj 2 var stat "+ ob2.varstatic);
		System.out.println(" obj 2 var instanta "+ ob2.vardeinstanta);
		

	}

}
