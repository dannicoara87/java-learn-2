package threaduri2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ClasaPrincipala extends Thread{
	
	public void run(){
		System.out.println("am inceput sa execut metoda run");
		
		List lista = new ArrayList();
		
		synchronized (lista ) {
			System.out.println("cod sincronizat intr-un bloc");
		}
	}

	public static void main(String[] args) {
		// thread principal
		System.out.println("incep rularea metodei main");
		
		// thread secundar - metoda clasica: start si run
		ClasaPrincipala thread1 = new ClasaPrincipala();
		thread1.setPriority(10);
		thread1.start();
		
		System.out.println("continui rularea metodei main");
		
		// tema: documentaat despre sleep, join, yield?
		
		// metoda cu inner class
		Thread thread2 = new Thread(new Runnable() {
			public void run() {
				System.out.println("rulez thread2");
			}
		});
		thread2.start();

	
	
		// sincronizari
		Lock test1 = new ReentrantLock();
		test1.lock();
		System.out.println("cod sincronizat");
		test1.unlock();
	
	}
	
	
	private synchronized void afisare(){
		System.out.println("Aafisare de text");
	}
	
	private static synchronized void afisareStatica(){
		System.out.println("Aafisare statica");
	}	

	
	

}
