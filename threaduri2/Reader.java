package threaduri2;

public class Reader extends Thread{
	
	Calculator c;
	int identificareThread;
	
	public Reader(Calculator calc, int iT){
		this.c = calc;
		this.identificareThread = iT;
	}
	public void run(){
		synchronized (c){
			try{
				System.out.println("astept calcularea. thread "+ identificareThread);
				c.wait();
			}
			catch(InterruptedException e){
				e.printStackTrace();
				
			}
			System.out.println("thread "+ identificareThread + ". suma este: "+ c.totalA);
		}
	}
	public static void main(String[] args) {
		Calculator calculator1 = new Calculator();
		
		new Reader(calculator1,1).start();
		new Reader(calculator1,2).start();
		new Reader(calculator1,3).start();
		new Reader(calculator1,4).start();
		
		calculator1.start();

	}

}
